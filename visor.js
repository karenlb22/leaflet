function initMap(){
    var mymap = L.map('visor').setView([4.643792, -74.172669], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', { attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibG9yZW5hMjIiLCJhIjoiY2tjd3I1ZzFrMGdxbjJ5cGRqcDVvaTJ0NyJ9.pjJcuNgGlsJI0s50hg793Q'
}).addTo(mymap);

var polygon = L.polygon([
    [4.646578255186662, -74.18569564819336],
    [4.624164168015378, -74.17642593383789], 
    [4.625704089861983, -74.17505264282227],
    [4.620228796979006, -74.17196273803711],
    [4.614240146973664, -74.17900085449219],
    [4.618688863234914, -74.18363571166992],
    [4.616122299564984, -74.18500900268555],
    [4.603802664823001, -74.17642593383789],
    [4.606369273003686, -74.1635513305664],
    [4.601064939225764, -74.1522216796875],
    [4.5952472373844175, -74.14981842041016],
    [4.594905018149873, -74.13848876953125],
    [4.637338946614495, -74.11857604980469],
    [4.649829094231134, -74.13230895996094],
    [4.663687765941434, -74.14037704467773],
    [4.663003393503384, -74.15874481201172],
    [4.646578255186662, -74.18569564819336]
]).addTo(mymap);

polygon.bindPopup("¡La mejor localidad de Bogotá!");

var marker = L.marker([4.643792, -74.172669]);
marker.addTo(mymap);

var circle = L.circle([4.643792, -74.172669], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.1,
    radius: 500
}).addTo(mymap);

marker.bindPopup("Mi Casa").openPopup();
circle.bindPopup("Patio Bonito");

var marker1 = L.marker([4.658056, -74.093889]);
marker1.addTo(mymap);

var circle1 = L.circle([4.658056, -74.093889], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.1,
    radius: 500
}).addTo(mymap);

marker1.bindPopup("Parque Simón Bólivar").openPopup();
circle1.bindPopup("Barrios Unidos");

var marker2 = L.marker([4.668056, -74.091389]);
marker2.addTo(mymap);

var circle2 = L.circle([4.668056, -74.091389], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.1,
    radius: 500
}).addTo(mymap);

marker2.bindPopup("Parque El Salitre").openPopup();
circle2.bindPopup("Barrios Unidos");

var marker3 = L.marker([4.597628, -74.081711]);
marker3.addTo(mymap);

var circle3 = L.circle([4.597628, -74.081711], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.1,
    radius: 500
}).addTo(mymap);

marker3.bindPopup("Parque Tercer Milenio").openPopup();
circle3.bindPopup("Santa Fe");

var marker4 = L.marker([4.597628, -74.081711]);
marker4.addTo(mymap);

var circle4 = L.circle([4.597628, -74.081711], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.1,
    radius: 500
}).addTo(mymap);

marker4.bindPopup("Parque Tercer Milenio").openPopup();
circle4.bindPopup("Santa Fe");



}
